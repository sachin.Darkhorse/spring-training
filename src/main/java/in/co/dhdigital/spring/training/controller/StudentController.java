package in.co.dhdigital.spring.training.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StudentController {

	@RequestMapping("/showCreate")
	public String showCreate() {
		return "createStudent";
	}
}
