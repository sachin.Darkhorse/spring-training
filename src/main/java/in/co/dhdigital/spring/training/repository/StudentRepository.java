package in.co.dhdigital.spring.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.dhdigital.spring.training.model.StudentModel;

public interface StudentRepository extends JpaRepository<StudentModel, Integer> {

}
