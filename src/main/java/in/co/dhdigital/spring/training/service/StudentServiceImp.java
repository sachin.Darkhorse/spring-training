package in.co.dhdigital.spring.training.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.dhdigital.spring.training.model.StudentModel;
import in.co.dhdigital.spring.training.repository.StudentRepository;

@Service
public class StudentServiceImp implements StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public void deleteStudent(StudentModel delStudentModel) {
		// TODO Auto-generated method stub
		studentRepository.delete(delStudentModel);
	}

	@Override
	public List<StudentModel> getAllStudent() {
		return studentRepository.findAll();
	}

	@Override
	public Optional<StudentModel> getStudentModelById(int id) {
		return studentRepository.findById(id);
	}

	public StudentRepository getStudentRepository() {
		return studentRepository;
	}

	@Override
	public StudentModel saveStudent(StudentModel saveStudentModel) {

		return studentRepository.save(saveStudentModel);
	}

	public void setStudentRepository(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	@Override
	public StudentModel updateStudent(StudentModel updateStudentModel) {
		// TODO Auto-generated method stub
		return studentRepository.save(updateStudentModel);
	}

}
