package in.co.dhdigital.spring.training.service;

import java.util.List;
import java.util.Optional;

import in.co.dhdigital.spring.training.model.StudentModel;

public interface StudentService {
	void deleteStudent(StudentModel delStudentModel);

	List<StudentModel> getAllStudent();

	Optional<StudentModel> getStudentModelById(int id);

	StudentModel saveStudent(StudentModel saveStudentModel);

	StudentModel updateStudent(StudentModel updateStudentModel);
}
