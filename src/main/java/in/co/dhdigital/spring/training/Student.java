package in.co.dhdigital.spring.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Student {
	// Demo Class Check
	public static void main(String[] args) {
		SpringApplication.run(Student.class, args);
	}

}
